<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_CONTACT US_email</name>
   <tag></tag>
   <elementGuidId>d3a53de8-256e-45be-a7ce-c25113bdb908</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='email']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;email&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>cedeb853-593b-4ba2-b628-02a80f88ff17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>b9b3d60a-8cad-4852-ba10-f61c08ae89bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>a71fe88c-0fec-4dbf-baf1-baa43655b554</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>feedback-input</value>
      <webElementGuid>e3084db9-4695-4529-986e-1d18a7a10e4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Email Address</value>
      <webElementGuid>8cc2c67c-fe1c-454e-8211-e73f7f7d6e8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;contact_form&quot;)/input[@class=&quot;feedback-input&quot;]</value>
      <webElementGuid>3de65836-e7f7-4e15-aed8-f94902ec304f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='email']</value>
      <webElementGuid>91d08db2-3832-4d14-8eb3-54d3753d9ffa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='contact_form']/input[3]</value>
      <webElementGuid>6349d0b5-dd02-49b0-85e1-a932cda6ffe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[3]</value>
      <webElementGuid>ed40e178-9498-4043-8ba5-a24928cf41f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'email' and @type = 'text' and @placeholder = 'Email Address']</value>
      <webElementGuid>abd4828c-1957-42ab-a47d-44ef9f80e3c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
