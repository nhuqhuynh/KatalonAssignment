<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Thank You for your Message</name>
   <tag></tag>
   <elementGuidId>348e3b49-1b07-40be-a511-a00d6755e907</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Thank You for your Message!' or . = 'Thank You for your Message!')]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='contact_reply']/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>21572219-ad45-4355-95ad-81e0747d8748</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Thank You for your Message!</value>
      <webElementGuid>41b09d80-2c7d-4a14-ba64-9ba3b343b746</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;contact_reply&quot;)/h1[1]</value>
      <webElementGuid>756782a6-ccc5-4247-832f-f9ff99ae7213</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contact_reply']/h1</value>
      <webElementGuid>3c9e4072-c7b6-48d1-bf5d-6114b3ceb445</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'contact_reply', '&quot;', ')/h1[1]')])[1]/preceding::h1[1]</value>
      <webElementGuid>0dbc5d66-88b0-4b59-81ec-64e2ff1f6768</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Thank You for your Message!']/parent::*</value>
      <webElementGuid>2a8c7383-08bf-486d-925a-6805e2e7c41e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>4cbe15f5-3a8c-41ec-bb2e-7d0576960276</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Thank You for your Message!' or . = 'Thank You for your Message!')]</value>
      <webElementGuid>298d79aa-ea39-4b85-bfc8-303ac66b19de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
