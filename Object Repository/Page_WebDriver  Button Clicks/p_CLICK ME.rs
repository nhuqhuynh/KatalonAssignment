<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_CLICK ME</name>
   <tag></tag>
   <elementGuidId>0399bfa6-221c-425e-9ce1-adeb1b2d4b42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='button1']/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>8083677d-bc42-46ac-8760-a9314c7ff9ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CLICK ME!</value>
      <webElementGuid>2e044d3c-8c0d-4fb5-a56c-4555edb677f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;button1&quot;)/p[1]</value>
      <webElementGuid>45341d07-799e-433c-9890-16dfdc3980f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='button1']/p</value>
      <webElementGuid>d2519c77-3e2d-40a2-b89d-c567a75a7444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='click().'])[1]/following::p[1]</value>
      <webElementGuid>63c362ab-6feb-491c-bee7-d768006a7268</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='JavaScript Click'])[1]/preceding::p[1]</value>
      <webElementGuid>c07aa51a-c0ea-46a9-9b11-1e5fec5098f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create a WebElement based on the button below.'])[2]/preceding::p[1]</value>
      <webElementGuid>c2169bab-3ccf-4394-a2cb-1a1ca4eb44ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CLICK ME!']/parent::*</value>
      <webElementGuid>c66cecae-31fa-4da8-a3a5-397dfd4df791</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>cd340e45-29e6-443d-b80b-8f042445c2c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'CLICK ME!' or . = 'CLICK ME!')]</value>
      <webElementGuid>d8ae6f80-2bcb-4107-aab9-d0074e7b3fee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
